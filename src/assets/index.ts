const assets = {
    images: {
        homeImage: require('../assets/img/home-image.png'),
        noDataFound: require('../assets/img/no-data-found.jpg')
    }
}

export default assets;