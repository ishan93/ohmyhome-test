// generate unique id
const getUniqueId = (): string => {
    const uniqueString = (Math.random().toString(36) + '00000000000000000').slice(2, 7);
    const currentTime = new Date().getTime();
    return `${uniqueString}-${Math.ceil(currentTime * Math.random())}`;
};

// store values to local storage
const storeInLocalStorage = (name: string, values: any): void => {
    localStorage.setItem(name, JSON.stringify(values));
};

// get local storage saved value by passing name
const getLocalStorageValue = (name: string): Object | Object[] => {
    const storageValues = localStorage.getItem(name);
    return storageValues ? JSON.parse(storageValues) : null;
};

// show currency by given currency format
const currencyFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

export { getUniqueId, storeInLocalStorage, getLocalStorageValue, currencyFormatter };