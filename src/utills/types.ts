interface IListingCard {
    id: string;
    title: string;
    description: string;
    amount: number;
    img?: string
}

export type { IListingCard };