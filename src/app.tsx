import React, { useEffect } from 'react';
import './app.scss';
import Route from './component/common/route';
import HomeDetails from './routes/home-details/home-details';
import Home from './routes/home/home';

function App() {
  useEffect(() => {}, []);
  return (
    <div className='app'>
      <Route path="/">
        <Home />
      </Route>
      <Route path="/home-details/:{id}">
        <HomeDetails />
      </Route>
    </div>
  );
}

export default App;
