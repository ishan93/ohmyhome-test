import React, { FC } from 'react';
import { currencyFormatter } from '../../utills';
import { IListingCard } from '../../utills/types';
import './home-block.scss';

const HomeBlock: FC<{data: IListingCard}> = ({data}) => {
  return (
    <div className="blockContainer">
      <img src={data.img} className='homeImage' />
      <div className='content'>
        <div className="title">{data.title}</div>
        <div className="description">{data.description}</div>
        <div className="price">{currencyFormatter.format(data.amount)}</div>
      </div>
    </div>
  );
};

export default HomeBlock;
