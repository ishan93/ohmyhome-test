import { useEffect, useState } from 'react';

interface IRoute {
  path: string;
  children: any;
}

const Route = ({ path, children }: IRoute) => {
  // state to track URL and force component to re-render on change
  const [currentPath, setCurrentPath] = useState(window.location.pathname);

  useEffect(() => {
    // define callback as separate function so it can be removed later with cleanup function
    const onLocationChange = () => {
      // update path state to current window URL
      setCurrentPath(window.location.pathname);
    };
    window.addEventListener('popstate', onLocationChange);
    // clean up event listener
    return () => {
      window.removeEventListener('popstate', onLocationChange);
    };
  }, []);

  const routeValidation = (): boolean => {
      let status: boolean = false;
      const routerSplit = window.location.pathname.split('/');
      const pathSplit = path.split('/');
      if(window.location.pathname === path) {
          status = true;
      }

      if(pathSplit[pathSplit.length - 1] === ':{id}') {
        if(routerSplit.length === 3 && routerSplit[1] === pathSplit[1]) {
            status = true;
        }
      }

    return status;
  }

  return routeValidation() ? children : null;
};

export default Route;
