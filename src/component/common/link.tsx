import React from 'react';

interface ILink {
  className?: string;
  href: string;
  children: any;
}

const Link = ({ className, href, children }: ILink) => {
  // prevent full page reload
  const onClick = (event: any) => {
    // if ctrl or meta key are held on click, allow default behavior of opening link in new tab
    if (event.metaKey || event.ctrlKey) {
      return;
    }

    event.preventDefault();
    // update url
    window.history.pushState({}, '', href);

    // communicate to Routes that URL has changed
    const navEvent = new PopStateEvent('popstate');
    window.dispatchEvent(navEvent);
  };
  return (
    <a className={className || ''} href={href} onClick={onClick}>
      {children}
    </a>
  );
};

export default Link;
