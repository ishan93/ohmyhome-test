import React, { useEffect, useState } from 'react';
import assets from '../../assets';
import Link from '../../component/common/link';
import HomeBlock from '../../component/home-block/home-block';
import {
  getLocalStorageValue,
  getUniqueId,
  storeInLocalStorage,
} from '../../utills';
import { IListingCard } from '../../utills/types';
import './home.scss'

const Home = () => {
  const [listingCards, setListingCards] = useState<IListingCard[]>([]);
  const [buttonEnable, setButtonEnable] = useState<boolean>(true);

  useEffect(() => {
    /*
     * check user has already stored listing cards in local storage or not
     * if user does, set them in state value
     * if not generate new 4 default listing cards
     */
    const listingCardsLocal: IListingCard[] =
      (getLocalStorageValue('listingCards') as IListingCard[]) || [];
    if (!(listingCardsLocal.length > 0)) {
      // create 4 listing cards with unique id
      Array(4)
        .fill(null)
        .map((val, index) => {
          const card: IListingCard = {
            id: getUniqueId(),
            title: `${index + 1} - Amber Sea`,
            amount: 1000000,
            description: 'Lorem ipsum dolor sit amet',
            img: assets.images.homeImage
          };
          listingCardsLocal.push(card);
        });
      storeInLocalStorage('listingCards', listingCardsLocal);
    }
    // set generated or stored listing card values to state
    setListingCards(listingCardsLocal);
  }, []);

  /*
  * add block click handler, listing card added between 5 seconds will be ignored
  * between this 5 seconds, button color will be change to disable
  */
  const addNewHomeBlock = (): void => {
    if (buttonEnable) {
      const card: IListingCard = {
        id: getUniqueId(),
        title: `${listingCards.length + 1} - Amber Sea`,
        amount: 1000000,
        description: 'Lorem ipsum dolor sit amet',
        img: assets.images.homeImage
      };
      setListingCards((prevState) => [...prevState, card]);
      storeInLocalStorage('listingCards', [...listingCards, card]);
      setButtonEnable(false);
      setTimeout(() => {
        setButtonEnable(true);
      }, 5000);
    }
  };

  return (
    <section className='wrapper'>
      <div className={`add-block-button button-${buttonEnable? 'enable' : 'disable'}`} onClick={addNewHomeBlock}>
        Add block
      </div>
      <div className='row'>
        {listingCards &&
          listingCards.length > 0 &&
          listingCards.map((card) => {
            return (
              <Link
                href={`/home-details/${card.id}`}
                key={card.id}
              >
                <div className='col-sm-12 col-md-6 col-lg-4 col-xl-3'>
                  <HomeBlock data={card}/>
                </div>
              </Link>
            );
          })}
      </div>
    </section>
  );
};

export default Home;
