import React, { useEffect, useState } from 'react';
import assets from '../../assets';
import { currencyFormatter, getLocalStorageValue } from '../../utills';
import { IListingCard } from '../../utills/types';
import './home-details.scss';

const HomeDetails = () => {
  const [cardData, setCardData] = useState<IListingCard>();
  useEffect(() => {
    const pathName = window.location.pathname;
    const cardId = pathName.split('/')[2];
    if (pathName && cardId) {
      /*
       * check router id param with saved listing card for match
       * if there is a match, it will save to state
       * otherwise it will show the no data found error message
       */
      const listingCardsLocal: IListingCard[] =
        (getLocalStorageValue('listingCards') as IListingCard[]) || [];
      const cardData = listingCardsLocal.find((card) => card.id === cardId);
      setCardData(cardData);
    }
  }, []);

  const ListingCardInfo = () => {
    return (
      <div>
        <h2>{cardData?.title}</h2>
        <img src={cardData?.img} alt='hello' className='homeImage' />

        <div className='card-details'>
          <div>{cardData?.description}</div>
          <div>
            {cardData?.amount && currencyFormatter.format(cardData?.amount)}
          </div>
        </div>
        {/* hardcoded data, will be replace with original description in future */}
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce
          sagittis eros ac lacus condimentum, vitae varius lectus dignissim.
          Quisque at enim elementum, dapibus tellus sed, ullamcorper lacus.
          Quisque scelerisque, sapien nec facilisis lobortis, lectus nibh
          molestie lacus, ac venenatis leo turpis nec neque. Praesent rhoncus
          neque in nisl molestie, id pretium sapien feugiat. Quisque vitae
          turpis risus. Vestibulum tempor, ligula quis tincidunt accumsan, neque
          dui mattis sem, in sagittis quam risus euismod libero. Nunc in massa
          in lorem rutrum varius vel a ex. Suspendisse mauris est, lobortis non
          ultrices a, tincidunt eu nisl. Fusce consequat eu nisl a ultrices.
        </p>

        <p>
          Maecenas rutrum cursus sagittis. Nam velit urna, auctor id blandit
          vel, blandit in arcu. Pellentesque vel dui gravida velit fringilla
          molestie non et justo. Maecenas eleifend eleifend lobortis. Nunc
          cursus aliquet vehicula. Duis id justo in purus rhoncus accumsan. Nam
          suscipit dolor ligula, scelerisque maximus nisi lobortis quis.
          Maecenas volutpat neque massa, at scelerisque lacus ornare a. Aliquam
          erat volutpat. Quisque fermentum sed tellus non dictum. Nam ornare
          faucibus nisi a suscipit. Nulla facilisi. Fusce iaculis libero non
          nunc porta, non aliquet lacus euismod.
        </p>
        <p>
          Class aptent taciti sociosqu ad litora torquent per conubia nostra,
          per inceptos himenaeos. Phasellus rutrum interdum tristique. Fusce ac
          tristique urna, vel mollis tortor. Ut tristique sapien ipsum, a
          vehicula urna eleifend vel. Quisque ligula sapien, venenatis nec nunc
          nec, lacinia fringilla felis. Vivamus ornare mollis tellus vitae
          varius. Mauris commodo arcu elementum fermentum tempus. Fusce interdum
          ornare rutrum. Aliquam luctus, ex eu facilisis sagittis, augue risus
          rhoncus lectus, sit amet auctor dolor elit sed neque. Aliquam
          condimentum gravida interdum. Morbi ultricies leo non auctor lacinia.
          Nunc et augue a turpis pharetra sodales sit amet fringilla turpis.
          Vivamus id justo neque. Vestibulum consequat lacus et dolor mollis,
          dignissim euismod urna lacinia. Lorem ipsum dolor sit amet,
          consectetur adipiscing elit. Proin vel venenatis leo.
        </p>
      </div>
    );
  };

  const ListingCardNotFound = () => {
    return (
      <div className='no-data-found'>
        <div className='error-title'>Sorry, No data found!</div>
        <div className='error-description'>
          Please check the url and try again.
        </div>
        <img src={assets.images.noDataFound} />
      </div>
    );
  };

  return (
    <section className='wrapper'>
      <div className='row container'>
        <div className='left-column col-md-8'>
          {cardData ? <ListingCardInfo /> : <ListingCardNotFound />}
        </div>
        <div className='right-column  col-md-4'>
          <div className='fake-img'>Fake Image</div>
          <div className='fake-img'>Fake Image</div>
          <div className='fake-img'>Fake Image</div>
        </div>
      </div>
    </section>
  );
};

export default HomeDetails;
